<?php

namespace App\Http\Controllers\RedirectLogin;

use App\Http\Controllers\Controller;
use App\Http\Service\LoginConnectionService;
use Illuminate\Http\Request;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;

class LoginController extends Controller
{

    public function login()
    {
        $status = 0;
        return view('login' )->with('status', $status);
    }

    public function board(Request $request)
    {
        Cookie::forget('authkey');
        $login = $request->get("login");
        $password = $request->get("pass");
        $service = new LoginConnectionService();
        //$requestFromServiceKsuz = $service->getKsuzRequest($login, $pasword);
        $req = $service->getBaseOrdersResponse($login, $password);
        $key = $req[1];
        $status = $req[0];
        if ($status == 202)
        {
            $response = new Response(view('board'));
            $response->cookie('authkey', $key, 45000);
            return $response;
        }
        $response = new Response(view('login')->with('status', $status));
        //$response = new Response(route)
        return $response;
    }
}
