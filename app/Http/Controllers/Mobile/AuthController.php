<?php
/**
 * Created by IntelliJ IDEA.
 * User: renegatumsoul
 * Date: 06.05.17
 * Time: 14:11
 */
namespace App\Http\Controllers\Mobile;
use App\Http\Controllers\Controller;
use App\Http\Service\MobileLoginService;

class AuthController extends Controller {

    // return 403 or key
    public function login($phone)
    {
        $mobile =  new MobileLoginService();
        return $mobile->loginRequest($phone);
    }

    //return ok or fall
    public function pass($phone, $key)
    {
        $mobile =  new MobileLoginService();
        return $mobile->passwordRequest($phone, $key);
    }
}