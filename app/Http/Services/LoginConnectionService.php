<?php
namespace App\Http\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Cookie;
use RuntimeException;

class LoginConnectionService
{

    private function loginResponse($login, $password, $uri) {
        $client = new Client;
        $status = 0;
        $authkey = "";
        try
        {
            $response = $client->get("$uri?user=$login&pass=$password");
            $response->getStatusCode();
            $json = json_decode($response->getBody(), true);
            $authkey = $json["authkey"];
            $status = 202;
        }
        catch (ClientException  $e)
        {
            switch ($e->getResponse()->getStatusCode())
            {
                case 403: $status = 403;
                    break;
                case 400: $status = 400;
                    break;
            }
        }
        catch (RequestException $e)
        {
            switch ($e->getResponse()->getStatusCode())
            {
                case 403: $status = 403;
                    break;
                case 400: $status = 400;
                    break;
            }
        }
        finally
        {
            return array($status,$authkey);
        }
    }

    public function getKsuzResponse($login, $password)
    {
        //Пока не доделает
        return true;
    }

    public function getBaseOrdersResponse($login, $password)
    {
        $uri = "http://test.buket116.ru/api/v1.0/auth/";
        return $this->loginResponse($login,$password,$uri);
    }
}