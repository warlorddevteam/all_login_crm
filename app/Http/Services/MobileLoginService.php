<?php
/**
 * Created by IntelliJ IDEA.
 * User: renegatumsoul
 * Date: 06.05.17
 * Time: 14:08
 */

namespace App\Http\Service;


use App\MobileUsers;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Mockery\Exception;
use function MongoDB\BSON\toJSON;
use Zend\Diactoros\Response;

class MobileLoginService
{

    public function loginRequest($phone)
    {
        $key = $this->generateKey();


        switch($this->checkLogin($phone, $key))
        {
            case 202: return response()->json(['key' => $key], 202);
            break;
            case 404: return response("no connection",404);
            break;
            case 403: return response("no phone",403);
            break;
        }
    }

    public function passwordRequest($phone, $key)
    {

        switch ($this->checkKey($phone, $key))
        {
            case 202: return response("ok", 202);
                break;
            case 404: return response("no connection",404);
                break;
            case 403: return response("invalid key",403);
                break;
        }
    }

    private function generateKey()
    {
        return random_int("1000", "9999");
    }

    private function    checkLogin($phone, $code)
    {
        $client = new Client();
        $status = 202;

        try {
            $response = $client->get("http://test.buket116.ru/api/v1.1/courier/login/?phone=$phone&confirm_code=$code");
            $response->getStatusCode();
        }
        catch (ClientException  $e)
        {
            switch ($e->getResponse()->getStatusCode())
            {
                case 403:  $status = 403;
                    break;
                case 400:  $status = 400;
                    break;
            }
        }
        catch (RequestException $e)
        {
            switch ($e->getResponse()->getStatusCode())
            {
                case 403:  $status = 403;
                    break;
                case 404:  $status = 404;
                    break;
            }
        }
        finally
        {
            return $status;
        }
    }

    private function checkKey($phone, $code)
    {
        $client = new Client();
        $status = 0;

        try {
            $response = $client->get("http://test.buket116.ru/api/v1.1/courier/check/?phone=$phone&confirm_code=$code");
            $response->getStatusCode();
            $status = 202;
        }
        catch (ClientException  $e)
        {
            switch ($e->getResponse()->getStatusCode())
            {
                case 403: $status = 403;
                    break;
                case 400: $status = 400;
                    break;
            }
        }
        catch (RequestException $e)
        {
            switch ($e->getResponse()->getStatusCode())
            {
                case 403: $status = 403;
                    break;
                case 400: $status = 400;
                    break;
            }
        }
        finally
        {
            return $status;
        }

        //http://test.buket116.ru/api/v1.1/courier/login/?phone=+79178883145&confirm_code=2343
    }
}