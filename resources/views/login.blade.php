@extends('layouts.app')
@section('content')
<div class = "container">
    <div class="wrapper">
        {{ Form::open(array('action'=> array('RedirectLogin\LoginController@board'), 'method' => 'post' , 'class' => 'form-signin')) }}
            <h3 class="form-signin-heading">Авторизация</h3>
            <hr class="colorgraph"><br>
            <input type="text" class="form-control" name="login" placeholder="Логин" required="" autofocus="" />
            <input type="password" class="form-control" name="pass" placeholder="Пароль" required=""/>
            <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Войти</button>
        {!! Form::close() !!}
    </div>
    @if ($status == 403)
    <script>
        $(document).ready(
            swal({
                title: "Неверный пароль",
                text: "Проверьте введенный вами пароль",
                type: "warning"}));
    </script>
    @endif
    @if ($status == 400)
    <script>
        $(document).ready(
            swal({
                title: "Ошибка авторизации",
                text: "Неправильное имя пользователя, попробуйте еще раз",
                type: "error"}));
    </script>
    @endif

</div>
@endsection
<style>
    .sweet-alert
    {
        background-color: #F5F8FA !important;

    }
    .alert{
        display:none;
    }
    .wrapper {
        margin-top: 80px;
        margin-bottom: 20px;
    }

    .form-signin {
        max-width: 420px;
        padding: 30px 38px 66px;
        margin: 0 auto;
        background-color: #eee;
        border: 3px dotted rgba(0,0,0,0.1);
    }

    .form-signin-heading {
        text-align:center;
        margin-bottom: 30px;
    }

    .form-control {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;
    }

    input[type="text"] {
        margin-bottom: 0px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }

    input[type="password"] {
        margin-bottom: 20px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .colorgraph {
        height: 7px;
        border-top: 0;
        background: #c4e17f;
        border-radius: 5px;
        background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
    }
</style>