<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return "ok";
//});
//
//Route::post('/', function () {
//    return "OK";
//});
//Route::get('/token', 'HomeController@token')->name('token');
//Route::get('/prot', function () {return "hi";})->middleware('auth');
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;

Route::group(['namespace' => 'RedirectLogin'], function () {
    Route::get('/', 'LoginController@login');
    Route::post('/board', 'LoginController@board');
});
Route::get('/pass/{phone}/{key}', 'TestController@pass');

Route::get('/test', 'TestController@index');

Route::get('/bz', function () {
    $authkey = Cookie::get("authkey");
    $response = new Response().redirect("http://test.buket116.ru/?authkey=$authkey");
    return $response;
});

Route::get('/ksyz', function () {
   return "oj";
}); 